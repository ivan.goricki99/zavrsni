<?php
    include 'core/init.php';
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">

        <title>Završni rad PWA - Ecommerce</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="manifest" href="manifest.json">

        <script src="assets/js/fetchApi.js"></script>
        <script src="assets/js/containerControl.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item active">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="caption">
                            <h2>Dobrodošli na LAP-TOP web trgovinu!</h2>
                            <div class="line-dec"></div>
                            <p>Ova e-commerce web stranica je napravila u svrhu prikaza funkcionalnosti PWA aplikacija za završni rad na temu <strong>Progresivne web aplikacije</strong></p>
                            <div class="main-button">
                                <a href="products.php">Pogledaj proizvode</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="featured-items">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Istaknuti proizvodi</h1>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="product-container" class="owl-carousel owl-theme">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="subscribe-form">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Pretplati se sada!</h1>
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2">
                        <div class="main-content">
                            <p>Kako bi u što kraćem roku saznao za nove proizvode na našem shopu pretplati se i svakog dana dobijav mail o novim proizvodima raspoloživim na našem webshopu</p>
                            <div class="container">
                                <form id="subscribe" action="" method="get">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <fieldset>
                                                <input name="email" type="text" class="placeholder-color-white form-input form-control" id="email-subscribe"
                                                       placeholder="E-mail adresa..." required="">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-5">
                                            <fieldset>
                                                <button type="button" onclick="containerControl.subscribeNewsletter()" id="email-subscribe-btn" class="button">Pretplati se</button>
                                            </fieldset>
                                        </div>
                                    </div>
                                </form>
                                <div id="error-container-subscribe"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>
        <script>
            document.addEventListener('DOMContentLoaded', function() {
                containerControl.navElementInit();
                containerControl.loadFeatured();
            });

            document.getElementById('subscribe').addEventListener('submit', function(e) {
                e.preventDefault();

                containerControl.subscribeNewsletter();
            });


        </script>
    </body>
</html>
