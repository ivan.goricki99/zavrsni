<?php
    include 'core/init.php';

    /*if($user->isLoggedin()) {
        unset($_SESSION['user']);
    }

    redirect('index.php');*/
?>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">
        <link rel="manifest" href="manifest.json">

        <script src="assets/js/fetchApi.js"></script>
        <script src="assets/js/containerControl.js"></script>

        <script>
            if(containerControl.isLoggedIn()) {
                containerControl.logout();
                containerControl.redirect('index.php');
            }
        </script>
    </head>
    <body>
    </body>
</html>