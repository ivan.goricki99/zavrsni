<?php
    include '../core/init.php';

    $data = extractDataFromUrl();
    $json = file_get_contents('php://input');
    $postData = json_decode($json);

    if(isset($postData) && isset($postData->method) && $postData->method === 'subscribe'){
        if($user->isSubscribed($postData->mail)){
            echo json_encode([
                "success" => 0,
                "message" => "Ovaj mail je već pretplačen!"
            ]);
            exit;
        }else{
            if($id = $user->subscribe($postData->mail)){
                echo json_encode([
                    "success" => 1
                ]);
                exit;
            }else{
                echo json_encode([
                    "success" => 0,
                    "message" => "Greška, pokušajte ponovo kasnije"
                ]);
                exit;
            }
        }
    }
