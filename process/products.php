<?php
    include '../core/init.php';

    $data = extractDataFromUrl();
    $json = file_get_contents('php://input');
    $postData = json_decode($json);

    if(isset($data) && isset($data['method']) && $data['method'] === 'getProducts'){
        if($allProducts = $product->getAll($data['limit'], $data['offset'], $data['sort'], $data['category'])){
            echo json_encode([
                "success" => 1,
                "data" => $allProducts
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Nema pdoataka"
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'featured'){
        if($allProducts = $product->getFeatured()){
            echo json_encode([
                "success" => 1,
                "data" => $allProducts
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Nema pdoataka"
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'similar'){
        if($allProducts = $product->getSimilar(9, $data['id'])){
            echo json_encode([
                "success" => 1,
                "data" => $allProducts
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Nema pdoataka"
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'getBasket'){
        if($allProducts = $product->getBasket($data['limit'], $data['offset'],
                                              $data['sort'], $data['category'],
                                              $data['user'])){
            echo json_encode([
                "success" => 1,
                "data" => $allProducts
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Nema pdoataka"
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'isInBasket'){
        if($isIn = $product->isInBasket($data['id'], $data['user'])){
            echo json_encode([
                "success" => 1,
                "basket" => $isIn
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 1,
                "basket" => 0
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'removeFromBasket'){
        if($change = $product->removeFromBasket($data['id'], $data['user'])){
            echo json_encode([
                "success" => 1,
                "change" => $change
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 1,
                "change" => 0
            ]);
            exit;
        }
    }

    if(isset($data) && isset($data['method']) && $data['method'] === 'addToBasket'){
        if($change = $product->addToBasket($data['id'], $data['user'])){
            echo json_encode([
                "success" => 1,
                "change" => $change
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 1,
                "change" =>0
            ]);
            exit;
        }
    }
