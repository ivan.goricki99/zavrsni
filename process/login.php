<?php
    include '../core/init.php';

    $data = extractDataFromUrl();
    $json = file_get_contents('php://input');
    $postData = json_decode($json);

    if(isset($postData) && isset($postData->method) && $postData->method === 'login'){
        if($id = $user->login($postData->username, $postData->password)){
            $_SESSION['user'] = $id;
            echo json_encode([
                "success" => $id
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Neispravno korisničko ime i lozinka"
            ]);
            exit;
        }
    }
