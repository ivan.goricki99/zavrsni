<?php
    include '../core/init.php';

    $data = extractDataFromUrl();
    $json = file_get_contents('php://input');
    $postData = json_decode($json);

    if(isset($postData) && isset($postData->method) && $postData->method === 'registration'){
        if(!$postData->mail) {
            echo json_encode([
                "success" => 0,
                "message" => "E-mail je obavezno polje"
            ]);
            exit;
        }else if($user->emailExist($postData->mail)) {
            echo json_encode([
                "success" => 0,
                "message" => "E-mail je u uporabi"
            ]);
            exit;
        }

        if(strlen($postData->username) < 4 && strlen($postData->username) > 20) {
            echo json_encode([
                "success" => 0,
                "message" => "Korisničko ime mora biti duže od 4 i krače od 20 znakova"
            ]);
            exit;
        }else if($user->usernameExist($postData->username)) {
            echo json_encode([
                "success" => 0,
                "message" => "Korisničko ime već postoji"
            ]);
            exit;
        }

        if(strlen($postData->password) < 4) {
            echo json_encode([
                "success" => 0,
                "message" => "Lozinka  mora biti duža od 4 znaka"
            ]);
            exit;
        }

        if($user->registration($postData->mail, $postData->username, $postData->password)){
            echo json_encode([
                "success" => 1
            ]);
            exit;
        }else{
            echo json_encode([
                "success" => 0,
                "message" => "Greška na serveru, kontaktirajte podršku"
            ]);
            exit;
        }
    }
