<?php
    include 'core/init.php';

    if(isset($_GET['page']) && is_numeric($_GET['page'])) {
        $page = $_GET['page'];
    }else{
        $page = 1;
    }

    if(isset($_GET['cat']) && is_numeric($_GET['cat'])) {
        $category = $_GET['cat'];
    }else{
        $category = null;
    }

    $limit = 9;
    $offset = ($page - 1) * $limit;

    $count = $product->getAllCount($category);
    $totalPages = ceil($count / $limit);

    if(isset($_GET['sort']) && in_array($_GET['sort'], ['newer', 'lower_price', 'higher_price'])) {
        $sort = $_GET['sort'];
    }else{
        $sort = 0;
    }

    $allProducts = $product->getAll($limit, $offset, $sort, $category);
    $allCategories = $product->getAllCategories();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">
        <link rel="manifest" href="manifest.json">

        <title>Proizvodi</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script src="assets/js/containerControl.js"></script>
        <script src="assets/js/fetchApi.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item active">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="featured-page">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-sm-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Proizvodi</h1>
                        </div>
                    </div>
                    <div class="col-md-8 col-sm-12">
                        <div id="filters" class="button-group">
                            <?php
                                if($sort && $sort === 'newer') {
                                    echo '
                                        <a href="products.php?sort=" class="selected-filter">Novo dodano</a>
                                    ';
                                }else{
                                    echo '
                                        <a href="products.php?sort=newer" class="btn btn-primary">Novo dodano</a>
                                    ';
                                }

                                if($sort && $sort === 'lower_price') {
                                    echo '
                                            <a href="products.php?sort=" class="selected-filter">Niža cijena</a>
                                        ';
                                }else{
                                    echo '
                                            <a href="products.php?sort=lower_price" class="btn btn-primary">Niža cijena</a>
                                        ';
                                }

                                if($sort && $sort === 'higher_price') {
                                    echo '
                                            <a href="products.php?sort=" class="selected-filter">Viša cijena</a>
                                        ';
                                }else{
                                    echo '
                                            <a href="products.php?sort=higher_price" class="btn btn-primary">Viša cijena</a>
                                        ';
                                }
                            ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div id="filters" class="button-group">
                        <?php
                            foreach ($allCategories as $categoryItem) {
                                echo ($categoryItem['id'] == $category ? '
                                    <a href="products.php" class="selected-filter btn btn-primary">'.$categoryItem['name'].'</a>
                                ' : '
                                    <a href="products.php?cat='.$categoryItem['id'].'" class="btn btn-primary">'.$categoryItem['name'].'</a>
                                ');
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>

        <div class="featured container no-gutter">
            <div class="row posts">
                <?php
                    foreach ($allProducts as $productItem) {
                        echo '
                                <div class="item new col-md-4">
                                    <a href="product.php?id='.$productItem['id'].'">
                                        <div class="featured-item">
                                            <img src="assets/images/products/'.$productItem['image'].'">
                                            <h4>'.$productItem['title'].'</h4>
                                            <h6>'.$productItem['price'].' kn</h6>
                                        </div>
                                    </a>
                                </div>
                            ';
                    }
                ?>
            </div>
        </div>

        <div class="page-navigation">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul>
                            <li class="<?php if($page <= 1){ echo 'disabled'; } ?>">
                                <a href="?page=1&sort=<?php echo $sort; ?>"><i class="fa fa-angle-double-left"></i></a>
                            </li>
                            <li class="<?php if($page <= 1){ echo 'disabled'; } ?>">
                                <a href="<?php if($page <= 1){ echo '#'; } else { echo "?page=".($page - 1); } ?>&sort=<?php echo $sort; ?>"><i class="fa fa-angle-left"></i></a>
                            </li>
                            <li class="<?php if($page >= $totalPages){ echo 'disabled'; } ?>">
                                <a href="<?php if($page >= $totalPages){ echo '#'; } else { echo "?page=".($page + 1); } ?>&sort=<?php echo $sort; ?>"><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li class="<?php if($page >= $totalPages){ echo 'disabled'; } ?>">
                                <a href="?page=<?php echo $totalPages; ?>&sort=<?php echo $sort; ?>"><i class="fa fa-angle-double-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>

        <script>
            containerControl.navElementInit();
           // containerControl.loadProducts();
        </script>
    </body>
</html>
