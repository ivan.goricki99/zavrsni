<?php
    include 'core/init.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="manifest" href="manifest.json">

        <title>Prijava</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script src="assets/js/containerControl.js"></script>
        <script src="assets/js/fetchApi.js"></script>
        <script>
            if(containerControl.isLoggedIn()) {
                containerControl.redirect('user.php');
            }
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="caption">
                            <h2>Registriraj se</h2>
                            <div class="line-dec"></div>
                            <form id="registration_form">
                                <ul class="list-group">
                                    <li>
                                        <p class="mb-0">E-mail</p>
                                        <input type="email"
                                               id="mail-registration-input"
                                               class="form-input form-control"
                                               name="mail"
                                               placeholder="Unesite adresu e-pošte">
                                    </li>
                                    <li>
                                        <p class="mb-0">Korisničko ime</p>
                                        <input type="text"
                                               id="username-registration-input"
                                               class="form-input form-control"
                                               name="username"
                                               placeholder="Unesite korisničko ime">
                                    </li>
                                    <li>
                                        <p class="mb-0">Lozinka</p>
                                        <input type="password"
                                               id="password-registration-input"
                                               class="form-input form-control"
                                               name="password"
                                               placeholder="Unesite lozinku">
                                    </li>
                                    <li>
                                        <p class="mb-0">Ponovi lozinku</p>
                                        <input type="password"
                                               id="password-repeat-registration-input"
                                               class="form-input form-control"
                                               name="password_repeat"
                                               onkeyup="checkMatch()"
                                               placeholder="Ponovite lozinku">
                                    </li>
                                    <li>
                                        <button type="submit"
                                                id="submit-registration-button"
                                                class="element__margin-top_bottom-10 main-button">Registriraj se</button>
                                    </li>
                                </ul>
                                <div id="registration-error-container"></div>
                            </form>
                            <p class="registration-info">Imate račun? <a href="login.php">Prijavi se</a>!</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Bootstrap core JavaScript -->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

        <!-- Additional Scripts -->
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>

        <script>
            document.getElementById('registration_form').addEventListener('submit', function () {
                let mail = document.getElementById('mail-registration-input');
                let username = document.getElementById('username-registration-input');
                let password = document.getElementById('password-registration-input');
                let passwordRepeat = document.getElementById('password-repeat-registration-input');
                let errorContainer = document.getElementById('registration-error-container');
                let submit = 1;

                containerControl.empty(errorContainer);
                containerControl.cleanInputError();

                if(!mail.value) {
                    errorContainer.innerHTML = '<p>E-mail je obavezno polje</p>';
                    containerControl.inputError(mail);
                    submit = 0;
                }

                if(username.value.length < 4 && username.value.length > 20) {
                    errorContainer.innerHTML = '<p>Korisničko ime mora biti duže od 4 i krače od 20 znakova</p>';
                    containerControl.inputError(username);
                    submit = 0;
                }

                if(password.value.length < 4) {
                    errorContainer.innerHTML = '<p>Lozinka  mora biti duža od 4 znaka</p>';
                    containerControl.inputError(password);
                    submit = 0;
                }else {
                    if(password.value !== passwordRepeat.value) {
                        errorContainer.innerHTML = '<p>Lozinke se ne podudaraju</p>';
                        containerControl.inputError(password);
                        containerControl.inputError(passwordRepeat);
                        submit = 0;
                    }
                }

                if(submit){
                    postData('process/registration.php', {
                        "method": "registration",
                        "mail": mail.value,
                        "username": username.value,
                        "password": password.value
                    }).then(data => {
                        if(data.success){
                            containerControl.redirect('index.php');
                        }else{
                            errorContainer.innerHTML = '<p>' + data.message + '</p>';
                        }
                    });
                }else {
                    errorContainer.innerHTML = '<p>Sva polja moraju biti ispunjena.</p>';
                }
            });

            function checkMatch() {
                let password = document.getElementById('password-registration-input');
                let passwordRepeat = document.getElementById('password-repeat-registration-input');

                containerControl.cleanInputError();

                if(password.value !== passwordRepeat.value) {
                    containerControl.inputError(password);
                    containerControl.inputError(passwordRepeat);
                }else{
                    containerControl.cleanInputError();
                }
            }

            containerControl.navElementInit();
        </script>
    </body>
</html>
