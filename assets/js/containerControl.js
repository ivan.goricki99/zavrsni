let containerControl = {
    borderColor: '#d1d1d1',
    empty: function(element) {
        element.innerHTML = '';
    },
    redirect: function(url) {
        window.location.href = url;
    },
    inputError: function (element) {
        element.style.borderColor = "red";
    },
    cleanInputError: function () {
        let elements = document.getElementsByClassName('form-input');
        let self = this;

        for(let i = 0; i < elements.length; i++){
            elements[i].style.borderColor = self.borderColor;
        }
    },
    getCookie: function(name) {
        const value = `; ${document.cookie}`;
        const parts = value.split(`; ${name}=`);
        if (parts.length === 2) return parts.pop().split(';').shift();
    },
    isLoggedIn: function() {
        let self = this;

        console.log(self.getCookie('user'));

        return self.getCookie('user') !== undefined ? 1 : 0;
    },
    logout: function() {
        let self = this;

        if(self.isLoggedIn()) {
            document.cookie = "user=" +
                ";expires=Thu, 01 Jan 1970 00:00:01 GMT";
        }
    },
    navElementInit: function() {
        let nav = document.getElementById('navUl');
        let self = this;
        const elements = document.getElementsByClassName('dynamic');
        while(elements.length > 0){
            elements[0].parentNode.removeChild(elements[0]);
        }

        if(self.isLoggedIn()) {
            let li = document.createElement('li');
            let a = document.createElement('a');
            li.classList.add('nav-item');
            a.classList.add('nav-link');
            a.setAttribute('href', "user.php");
            a.innerText = 'Korisnički profil';
            li.appendChild(a);
            nav.appendChild(li);

            li = document.createElement('li');
            a = document.createElement('a');
            li.classList.add('nav-item');
            a.classList.add('nav-link');
            a.setAttribute('href', "basket.php");
            a.innerText = 'Košarica';
            li.appendChild(a);
            nav.appendChild(li)

            li = document.createElement('li');
            a = document.createElement('a');
            li.classList.add('nav-item');
            a.classList.add('nav-link');
            a.setAttribute('href', "logout.php");
            a.innerText = 'Odjava';
            li.appendChild(a);
            nav.appendChild(li);

        }else {
            let li = document.createElement('li');
            let a = document.createElement('a');
            li.classList.add('nav-item');
            a.classList.add('nav-link');
            a.setAttribute('href', "login.php");
            a.innerText = 'Prijava / Registracija';
            li.appendChild(a);
            nav.appendChild(li);

            li = document.createElement('li');
            a = document.createElement('a');
            li.classList.add('nav-item');
            a.classList.add('nav-link');
            a.setAttribute('href', "about.php");
            a.innerText = 'O nama';
            li.appendChild(a);
            nav.appendChild(li);
        }
    },
    loadProducts: function () {
        getData('process/products.php', {
            method: "getProducts",
            limit: 9,
            offset: 0,
            sort: 0,
            category: 0
        }).then(data => {
            if(data.success){
                const productss = $('.posts');
                productss.empty();

                for(let i = 0; data.data.length; i++) {
                    if(data.data[i].id)
                        productss.append('<div class="item new col-md-4"><a href="product.php?id='+data.data[i].id+'"><div class="featured-item"><img src="assets/images/products/'+data.data[i].image+'"> <h4>'+data.data[i].title+'</h4><h6>'+data.data[i].price+' kn</h6></div></a></div>');
                }
            };
        });
    },
    loadFeatured: function () {
        getData('process/products.php', {
            method: "featured"
        }).then(data => {
            if(data.success){
                const productss = $('#product-container');
                const owl = $('#product-container');
                let content;
                let t = 0;
                productss.empty();

                for(let i = 0; data.data.length; i++) {
                    productss.append('<a href="product.php?id='+data.data[i].id+'"><div class="featured-item"><img src="assets/images/products/'+data.data[i].image+'"><h4>'+data.data[i].title+'</h4><h6>'+data.data[i].price+' kn</h6></div> </a>');

                    if(t === 0) {
                        setInterval(function () {
                            owl.owlCarousel({
                                items: 4,
                                lazyLoad: true,
                                loop: true,
                                dots: true,
                                margin: 20,
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 1,
                                    },
                                    600: {
                                        items: 2,
                                    },
                                    1000: {
                                        items: 4,
                                    }
                                }
                            });
                        }, 500);
                    }
                    t = 1;
                }
            }
        });
    },
    loadSimilar: function(id) {
        getData('process/products.php', {
            method: "similar",
            id: id
        }).then(data => {
            if(data.success){
                const productss = $('#product-container');
                const owl = $('#product-container');
                let content;
                let t = 0;
                productss.empty();

                for(let i = 0; data.data.length; i++) {
                    productss.append('<a href="product.php?id='+data.data[i].id+'"><div class="featured-item"><img src="assets/images/products/'+data.data[i].image+'"><h4>'+data.data[i].title+'</h4><h6>'+data.data[i].price+' kn</h6></div> </a>');

                    if(t === 0) {
                        setInterval(function () {
                            owl.owlCarousel({
                                items: 4,
                                lazyLoad: true,
                                loop: true,
                                dots: true,
                                margin: 20,
                                responsiveClass: true,
                                responsive: {
                                    0: {
                                        items: 1,
                                    },
                                    600: {
                                        items: 2,
                                    },
                                    1000: {
                                        items: 4,
                                    }
                                }
                            });
                        }, 500);
                    }
                    t = 1;
                }
            }
        });
    },
    loadBasket: function (limit = 9, offset = 0, sort = 0, category = 0) {
        let self = this;

        if(self.isLoggedIn()) {
            getData('process/products.php', {
                method: "getBasket",
                limit: limit,
                offset: offset,
                sort: sort,
                category: category,
                user: self.getCookie('user')
            }).then(data => {
                console.log(data);
                if(data.success){
                    const productss = $('.posts');
                    productss.empty();

                    for(let i = 0; data.data.length; i++) {
                        if(data.data[i].id)
                            productss.append('<div class="item new col-md-4"><a href="product.php?id='+data.data[i].id+'"><div class="featured-item"><img src="assets/images/products/'+data.data[i].image+'"> <h4>'+data.data[i].title+'</h4><h6>'+data.data[i].price+' kn</h6></div></a></div>');
                    }
                };
            });
        }
    },
    initBasketButton: function (id) {
        let self = this;
        getData('process/products.php', {
            method: "isInBasket",
            id: id,
            user: self.getCookie('user')
        }).then(data => {
            console.log(data);
            let addBasket = document.getElementById('add-basket');
            let removeBasket = document.getElementById('remove-basket');

            if(data.success){
                if(data.basket) {
                    addBasket.classList.add('displayNone');
                    removeBasket.classList.remove('displayNone');
                }else{
                    addBasket.classList.remove('displayNone');
                    removeBasket.classList.add('displayNone');
                }
            }
        });
    },
    removeFromBasket: function (product) {
        let self = this;
        getData('process/products.php', {
            method: "removeFromBasket",
            id: product,
            user: self.getCookie('user')
        }).then(data => {
            console.log(data);
            let addBasket = document.getElementById('add-basket');
            let removeBasket = document.getElementById('remove-basket');

            if(data.success){
                if(data.change) {
                    addBasket.classList.remove('displayNone');
                    removeBasket.classList.add('displayNone');
                }
            }
        });
    },
    addToBasket: function (product) {
        let self = this;
        getData('process/products.php', {
            method: "addToBasket",
            id: product,
            user: self.getCookie('user')
        }).then(data => {
            console.log(data);
            let addBasket = document.getElementById('add-basket');
            let removeBasket = document.getElementById('remove-basket');

            if(data.success){
                if(data.change) {
                    addBasket.classList.add('displayNone');
                    removeBasket.classList.remove('displayNone');
                }
            }
        });
    },
    subscribeNewsletter: function() {
        let mail = document.getElementById('email-subscribe');
        let button = document.getElementById('email-subscribe-btn');
        let errorContainer = document.getElementById('error-container-subscribe');
        let self = this;

        button.classList.remove('button-success-color');
        button.classList.remove('button-error-color');
        self.empty(errorContainer);

        containerControl.cleanInputError();
        if(mail.value.length < 8) {
            containerControl.inputError(mail);
        }else {
            postData('process/subscribe.php', {
                "method": "subscribe",
                "mail": mail.value
            }).then(data => {
                if(data.success){
                    button.innerText = 'Pretplaćeni ste!';
                    button.classList.add('button-success-color');
                }else{
                    button.classList.add('button-error-color');
                    errorContainer.innerHTML = '<p style="color: #fff;">' + data.message + '</p>';
                }
            });
        }
    },
    createNotification(title, body) {
        let self = this;
        const notifTitle = title;
        const notifBody = body;
        const notifImg = "assets/images/icons/icon_192.png";
        const options = {
            body: notifBody,
            icon: notifImg,
        };
        new Notification(notifTitle, options);

        console.log('notify triggered');
    }
};

if ("serviceWorker" in navigator) {
    navigator.serviceWorker.register("service-worker.js")
        .then(function (reg){
            var serviceWorker;
            if (reg.installing) {
                serviceWorker = reg.installing;
            } else if (reg.waiting) {
                serviceWorker = reg.waiting;
            } else if (reg.active) {
                serviceWorker = reg.active;
            }

            if (serviceWorker) {
                containerControl.createNotification("Lap-Top trgovina", "Dobrodošli na Lap-Top trgovinu, veliki izbor prijenosnih i PC računala!");
            }
        }).catch(function(e) {
            console.log("Failed: " + e);
        });
}