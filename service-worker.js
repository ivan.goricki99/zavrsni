/**
 * Copyright 2016 Google Inc. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
*/

// DO NOT EDIT THIS GENERATED OUTPUT DIRECTLY!
// This file should be overwritten as part of your build process.
// If you need to extend the behavior of the generated service worker, the best approach is to write
// additional code and include it using the importScripts option:
//   https://github.com/GoogleChrome/sw-precache#importscripts-arraystring
//
// Alternatively, it's possible to make changes to the underlying template file and then use that as the
// new base for generating output, via the templateFilePath option:
//   https://github.com/GoogleChrome/sw-precache#templatefilepath-string
//
// If you go that route, make sure that whenever you update your sw-precache dependency, you reconcile any
// changes made to this original template file with your modified copy.

// This generated service worker JavaScript will precache your site's resources.
// The code needs to be saved in a .js file at the top-level of your site, and registered
// from your pages in order to be used. See
// https://github.com/googlechrome/sw-precache/blob/master/demo/app/js/service-worker-registration.js
// for an example of how you can register this script and handle various service worker events.

/* eslint-env worker, serviceworker */
/* eslint-disable indent, no-unused-vars, no-multiple-empty-lines, max-nested-callbacks, space-before-function-paren, quotes, comma-spacing */
'use strict';

var precacheConfig = [
    ["assets/css/flex-slider.css", "f42a1c5c2c8c7c64bcab84f863cf5eda"],
    ["assets/css/fontawesome.css", "f667e6132f8470a39d2395b81ab4ef09"],
    ["assets/css/owl.css", "490ded165b3b667eec316eb4fb131407"],
    ["assets/css/style.css", "33c2f67f252e07ba6d8aefdd36469074"],
    ["assets/css/tooplate-main.css", "209a9c33fac3cccbc4d6751f2d1dbce7"],
    ["assets/fonts/Flaticon.woff", "396f6e0764e4393f8f3f46056258c9c7"],
    ["assets/fonts/FontAwesome.otf", "0b462f5cc07779cab3bef252c0271f2b"],
    ["assets/fonts/flexslider-icon.eot", "9c9cb7a6055043933ba68854f521af45"],
    ["assets/fonts/flexslider-icon.svg", "10e8a5455c4522c48aa975eacd4f0023"],
    ["assets/fonts/flexslider-icon.ttf", "b4c9e5057989b9727a5df4e0a21af33c"],
    ["assets/fonts/flexslider-icon.woff", "f8b92f66539473eea649c8514eb836a0"],
    ["assets/fonts/fontawesome-webfont.eot", "f7c2b4b747b1a225eb8dee034134a1b0"],
    ["assets/fonts/fontawesome-webfont.svg", "2980083682e94d33a66eef2e7d612519"],
    ["assets/fonts/fontawesome-webfont.ttf", "706450d7bba6374ca02fe167d86685cb"],
    ["assets/fonts/fontawesome-webfont.woff", "d9ee23d59d0e0e727b51368b458a0bff"],
    ["assets/fonts/fontawesome-webfont.woff2", "97493d3f11c0a3bd5cbd959f5d19b699"],
    ["assets/fonts/slick.eot", "ced611daf7709cc778da928fec876475"],
    ["assets/fonts/slick.svg", "f97e3bbf73254b0112091d0192f17aec"],
    ["assets/fonts/slick.ttf", "d41f55a78e6f49a5512878df1737e58a"],
    ["assets/fonts/slick.woff", "b7c9e1e479de3b53f1e4e30ebac2403a"],
    ["assets/images/5d4ed0c6cacac_thumb900.webp", "bb72880cabce5a1ec125629559df4a44"],
    ["assets/images/header-logo.png", "e9eaa4d52bc9bd2b06c316f3de5082a1"],
    ["assets/images/icons/icon_192.png", "7873db742c2fc26dfa81f760f9d0dc20"],
    ["assets/images/icons/icon_512.png", "384f6b88d1d09f007e77986980964ccc"],
    ["assets/images/products/dfreth657kmtn.jpg", "b47efa53f445aaa0a032da2db4f96c19"],
    ["assets/images/products/fujitsu-esprimo-p756-i7-6700-16gb-ddr4-256gb-ssd-2tb-winpro-rabljeni-uredaj.jpg", "05c063f8383c21682e454c15ee441edf"],
    ["assets/images/products/hp-elitebook-folio-1040-g3-2k-touch-i5-6300-8gb-ddr4-256gb-ssd-rabljeni-uredaj.jpg", "ac663fbbafcded3ff1970f7f03658cec"],
    ["assets/images/products/hp-prijenosno-racunalo-15s-eq1016nm-1n7z7ea.jpg", "65c5ed7617641b01c75452dd7d202a2c"],
    ["assets/images/products/hp-zbook-15-g3-core-i7-6820hq-3-60ghz-32gb-ddr4-512gb-ssd-winpro-rabljeni-uredaj.jpg", "714fd39ca5c150eba8b256b1a144b8b5"],
    ["assets/images/products/hp-zbook-15u-g3-core-i7-6600u-16gb-ddr4-512gb-ssd-rabljeni-uredaj.jpg", "2e1d8f4117e30db656ca029dc20c805c"],
    ["assets/images/products/kuciste-asus-gt501-tuf-gaming.jpg", "9559b51b2f25f0eee71b96a4400c381a"],
    ["assets/images/products/latitude-5520-15-6-fhd-i5-1135g7-8gb-s256gb-int-w10pro-gry-3y.jpg", "c870102d7e1e9c5472a541ac5d382b99"],
    ["assets/images/products/lenovo-m72e-i3-22-monitor-rabljeni-uredaj.jpg", "47472e06644fd904f08a7c911c02af78"],
    ["assets/images/products/lenovo-prijenosno-racunalo-yoga-7-14itl5-82bh007ysc.jpg", "62dfcafb19e0de227dd17b20a293df2a"],
    ["assets/images/products/lenovo-thinkpad-l450-core-i3-rabljeni-uredaj.jpg", "ccdf21d029526a386ba330e9d2bf2474"],
    ["assets/images/products/lenovo-thinkpad-l470-core-i3-6100u-8gb-ddr4-240gb-ssd-rabljeni-uredaj.jpg", "5b29cc33da6e033f0902f042080166c3"],
    ["assets/images/products/lenovo-thinkpad-x250-i5-5300-8gb-ddr3-240gb-ssd-winpro-rabljeni-uredaj.jpg", "e11210d26f3e07a4900ec59199157a77"],
    ["assets/js/containerControl.js", "1fcfa0677d9ffda8be9a42024a9e7b3d"],
    ["assets/js/custom.js", "4db4bccb4638f24234d0c7b3c17954f6"],
    ["assets/js/fetchApi.js", "b48e3345e12d1e8509e9a09a16006e85"],
    ["assets/js/flex-slider.js", "906c3ada1ed07698ef53c03300e37368"],
    ["assets/js/isotope.js", "9014a4cffd38cc32bc70baab0dc3a7c9"],
    ["assets/js/owl.js", "2fd359627020c93d2e7706075fb56a21"],
    ["vendor/bootstrap/css/bootstrap.css", "d26ecc887c12f855a908679dae6704e3"],
    ["vendor/bootstrap/css/bootstrap.css.map", "0ab7c5a3c60c431b89b5a1e3fb26c525"],
    ["vendor/bootstrap/css/bootstrap.min.css", "04aca1f4cd3ec3c05a75a879f3be75a3"],
    ["vendor/bootstrap/css/bootstrap.min.css.map", "13864e70f3d7eb9e606ee6bf6ce3451a"],
    ["vendor/bootstrap/js/bootstrap.bundle.js", "50a98c751c19ae5ea4fc42b2ba2da89b"],
    ["vendor/bootstrap/js/bootstrap.bundle.js.map", "6099416e857ce20c2dc67e5c70e31c10"],
    ["vendor/bootstrap/js/bootstrap.bundle.min.js", "ef58fee438cd2da2c3b33ff6f1cfeebf"],
    ["vendor/bootstrap/js/bootstrap.bundle.min.js.map", "45e94ecc39062646ee2f86d8b550e20f"],
    ["vendor/bootstrap/js/bootstrap.js", "4bc939cd6b79a562e8d14bc7a4674520"],
    ["vendor/bootstrap/js/bootstrap.js.map", "c38911715a82d955e1f61dabb031a18d"],
    ["vendor/bootstrap/js/bootstrap.min.js", "67176c242e1bdc20603c878dee836df3"],
    ["vendor/bootstrap/js/bootstrap.min.js.map", "2fc279b4cd4ace33a72aa2ae05a83704"],
    ["vendor/jquery/jquery.js", "6a07da9fae934baf3f749e876bbfdd96"],
    ["vendor/jquery/jquery.min.js", "4b57cf46dc8cb95c4cca54afc85e9540"],
    ["vendor/jquery/jquery.min.map", "bae3c738b74dd89a555b7a54e4891608"],
    ["vendor/jquery/jquery.slim.js", "450d478c0491cf0b2d365997faff70dd"],
    ["vendor/jquery/jquery.slim.min.js", "99b0a83cf1b0b1e2cb16041520e87641"],
    ["vendor/jquery/jquery.slim.min.map", "375e0272b0153d6871979c5ac2465321"],
];

var cacheName = 'sw-precache-v3-sw-precache-' + (self.registration ? self.registration.scope : '');


var ignoreUrlParametersMatching = [/^utm_/];



var addDirectoryIndex = function(originalUrl, index) {
    var url = new URL(originalUrl);
    if (url.pathname.slice(-1) === '/') {
      url.pathname += index;
    }
    return url.toString();
  };

var cleanResponse = function(originalResponse) {
    // If this is not a redirected response, then we don't have to do anything.
    if (!originalResponse.redirected) {
      return Promise.resolve(originalResponse);
    }

    // Firefox 50 and below doesn't support the Response.body stream, so we may
    // need to read the entire body to memory as a Blob.
    var bodyPromise = 'body' in originalResponse ?
      Promise.resolve(originalResponse.body) :
      originalResponse.blob();

    return bodyPromise.then(function(body) {
      // new Response() is happy when passed either a stream or a Blob.
      return new Response(body, {
        headers: originalResponse.headers,
        status: originalResponse.status,
        statusText: originalResponse.statusText
      });
    });
  };

var createCacheKey = function(originalUrl, paramName, paramValue,
                           dontCacheBustUrlsMatching) {
    // Create a new URL object to avoid modifying originalUrl.
    var url = new URL(originalUrl);

    // If dontCacheBustUrlsMatching is not set, or if we don't have a match,
    // then add in the extra cache-busting URL parameter.
    if (!dontCacheBustUrlsMatching ||
        !(url.pathname.match(dontCacheBustUrlsMatching))) {
      url.search += (url.search ? '&' : '') +
        encodeURIComponent(paramName) + '=' + encodeURIComponent(paramValue);
    }

    return url.toString();
  };

var isPathWhitelisted = function(whitelist, absoluteUrlString) {
    // If the whitelist is empty, then consider all URLs to be whitelisted.
    if (whitelist.length === 0) {
      return true;
    }

    // Otherwise compare each path regex to the path of the URL passed in.
    var path = (new URL(absoluteUrlString)).pathname;
    return whitelist.some(function(whitelistedPathRegex) {
      return path.match(whitelistedPathRegex);
    });
  };

var stripIgnoredUrlParameters = function(originalUrl,
    ignoreUrlParametersMatching) {
    var url = new URL(originalUrl);
    // Remove the hash; see https://github.com/GoogleChrome/sw-precache/issues/290
    url.hash = '';

    url.search = url.search.slice(1) // Exclude initial '?'
      .split('&') // Split into an array of 'key=value' strings
      .map(function(kv) {
        return kv.split('='); // Split each 'key=value' string into a [key, value] array
      })
      .filter(function(kv) {
        return ignoreUrlParametersMatching.every(function(ignoredRegex) {
          return !ignoredRegex.test(kv[0]); // Return true iff the key doesn't match any of the regexes.
        });
      })
      .map(function(kv) {
        return kv.join('='); // Join each [key, value] array into a 'key=value' string
      })
      .join('&'); // Join the array of 'key=value' strings into a string with '&' in between each

    return url.toString();
  };


var hashParamName = '_sw-precache';
var urlsToCacheKeys = new Map(
  precacheConfig.map(function(item) {
    var relativeUrl = item[0];
    var hash = item[1];
    var absoluteUrl = new URL(relativeUrl, self.location);
    var cacheKey = createCacheKey(absoluteUrl, hashParamName, hash, false);
    return [absoluteUrl.toString(), cacheKey];
  })
);

function setOfCachedUrls(cache) {
  return cache.keys().then(function(requests) {
    return requests.map(function(request) {
      return request.url;
    });
  }).then(function(urls) {
    return new Set(urls);
  });
}

self.addEventListener('install', function(event) {
  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return setOfCachedUrls(cache).then(function(cachedUrls) {
        return Promise.all(
          Array.from(urlsToCacheKeys.values()).map(function(cacheKey) {
            // If we don't have a key matching url in the cache already, add it.
            if (!cachedUrls.has(cacheKey)) {
              var request = new Request(cacheKey, {credentials: 'same-origin'});
              return fetch(request).then(function(response) {
                // Bail out of installation unless we get back a 200 OK for
                // every request.
                if (!response.ok) {
                  throw new Error('Request for ' + cacheKey + ' returned a ' +
                    'response with status ' + response.status);
                }

                return cleanResponse(response).then(function(responseToCache) {
                  return cache.put(cacheKey, responseToCache);
                });
              });
            }
          })
        );
      });
    }).then(function() {

      // Force the SW to transition from installing -> active state
      return self.skipWaiting();

    })
  );
});

self.addEventListener('activate', function(event) {
  var setOfExpectedUrls = new Set(urlsToCacheKeys.values());

  event.waitUntil(
    caches.open(cacheName).then(function(cache) {
      return cache.keys().then(function(existingRequests) {
        return Promise.all(
          existingRequests.map(function(existingRequest) {
            if (!setOfExpectedUrls.has(existingRequest.url)) {
              return cache.delete(existingRequest);
            }
          })
        );
      });
    }).then(function() {
      
      return self.clients.claim();
      
    })
  );
});


self.addEventListener('fetch', function(event) {
  if (event.request.method === 'GET') {
    // Should we call event.respondWith() inside this fetch event handler?
    // This needs to be determined synchronously, which will give other fetch
    // handlers a chance to handle the request if need be.
    var shouldRespond;

    // First, remove all the ignored parameters and hash fragment, and see if we
    // have that URL in our cache. If so, great! shouldRespond will be true.
    var url = stripIgnoredUrlParameters(event.request.url, ignoreUrlParametersMatching);
    shouldRespond = urlsToCacheKeys.has(url);

    // If shouldRespond is false, check again, this time with 'index.html'
    // (or whatever the directoryIndex option is set to) at the end.
    var directoryIndex = 'index.html';
    if (!shouldRespond && directoryIndex) {
      url = addDirectoryIndex(url, directoryIndex);
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond is still false, check to see if this is a navigation
    // request, and if so, whether the URL matches navigateFallbackWhitelist.
    var navigateFallback = '';
    if (!shouldRespond &&
        navigateFallback &&
        (event.request.mode === 'navigate') &&
        isPathWhitelisted([], event.request.url)) {
      url = new URL(navigateFallback, self.location).toString();
      shouldRespond = urlsToCacheKeys.has(url);
    }

    // If shouldRespond was set to true at any point, then call
    // event.respondWith(), using the appropriate cache key.
    if (shouldRespond) {
      event.respondWith(
        caches.open(cacheName).then(function(cache) {
          return cache.match(urlsToCacheKeys.get(url)).then(function(response) {
            if (response) {
              return response;
            }
            throw Error('The cached response that was expected is missing.');
          });
        }).catch(function(e) {
          // Fall back to just fetch()ing the request if some unexpected error
          // prevented the cached response from being valid.
          console.warn('Couldn\'t serve response for "%s" from cache: %O', event.request.url, e);
          return fetch(event.request);
        })
      );
    }
  }
});

self.addEventListener('push', function(event) {
    const payload = event.data ? event.data.text() : 'no payload';
    event.waitUntil(
        self.registration.showNotification('Lap-Top notifikacija', {
            body: payload,
        })
    );
});






