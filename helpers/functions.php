<?php
    function redirect($url) {
	    header('Location: ' . $url);
    }

    function getFirst($string) {
	    return explode(',', $string)[0];
    }
