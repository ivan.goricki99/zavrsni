<?php
    function extractDataFromUrl(): array {
        $return = [];

        $data = parse_url($_SERVER['REQUEST_URI'], PHP_URL_QUERY);
        $data = explode("&", $data);

        if(!empty($data)){
            foreach($data as $val){
                $tmp = explode( '=', $val );

                if(isset($tmp[1])) $return[$tmp[0]] = $tmp[1];
            }
        }

        return $return;
    }
