<?php

	Class Errors{
		private $db;

		function __construct($pdo){
			$this->db = $pdo;
		}

		public function saveError(string $table, string $from, Array $message): Array{
			$dateOccurred = date("Y-m-d H:i:s");

			$messageToString = implode(' | ', $message);

			$stmt = $this->db->prepare("INSERT INTO mysql_errors (table_occurred, fromm, message, date_occurred) VALUES (:table_occurred, :fromm, :message, :date_occurred)");
			$stmt->bindParam(":table_occurred", $table, PDO::PARAM_STR);
			$stmt->bindParam(":fromm", $from, PDO::PARAM_STR);
			$stmt->bindParam(":message", $messageToString, PDO::PARAM_STR);
			$stmt->bindParam(":date_occurred", $dateOccurred, PDO::PARAM_STR);
			$stmt->execute();

			return $stmt->errorInfo();
		}
	}
