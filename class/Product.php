<?php

    class Product {
        private $db;
        private $error;

        function __construct($pdo, $error){
            $this->db = $pdo;
            $this->err = $error;
        }

        public function getAll($limit,$offset, $sort, $category) {
            $categoryQuery = '';
            $sortQuery = '';

            if($sort && in_array($sort, ['newer', 'lower_price', 'higher_price'])) {
                if($sort === 'newer') {
                    $sortQuery = ' ORDER BY added_Date DESC';
                }else if($sort === 'lower_price') {
                    $sortQuery = ' ORDER BY price ASC';
                }else if($sort === 'higher_price') {
                    $sortQuery = ' ORDER BY price DESC';
                }
            }

            if($category) {
                $categoryQuery = ' AND category = '.$category;
            }

            $stmt = $this->db->prepare("SELECT * FROM products WHERE deleted = 0".$categoryQuery.$sortQuery." LIMIT :offset, :limit");
            $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
            $stmt->bindParam(":offset", $offset, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('products', 'Product::getAll', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getBasket($limit,$offset, $sort, $category, $user) {
            $categoryQuery = '';
            $sortQuery = '';

            if($sort && in_array($sort, ['newer', 'lower_price', 'higher_price'])) {
                if($sort === 'newer') {
                    $sortQuery = ' ORDER BY p.added_Date DESC';
                }else if($sort === 'lower_price') {
                    $sortQuery = ' ORDER BY p.price ASC';
                }else if($sort === 'higher_price') {
                    $sortQuery = ' ORDER BY p.price DESC';
                }
            }

            if($category) {
                $categoryQuery = ' AND p.category = '.$category;
            }

            $stmt = $this->db->prepare("SELECT p.* FROM products p JOIN basket b ON p.id = b.product WHERE p.deleted = 0 AND b.user = :user".$categoryQuery.$sortQuery." LIMIT :offset, :limit");
            $stmt->bindParam(":user", $user, PDO::PARAM_INT);
            $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
            $stmt->bindParam(":offset", $offset, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('products', 'Product::getAll', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getBasketCount($category, $user) {
            $categoryQuery = '';

            if($category) {
                $categoryQuery = ' AND p.category = '.$category;
            }

            $stmt = $this->db->prepare("SELECT p.id FROM products p JOIN basket b ON p.id = b.product WHERE p.deleted = 0 AND b.user = :user".$categoryQuery);
            $stmt->bindParam(":user", $user, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('products', 'Product::getAll', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function getAllCount($category) {
            $categoryQuery = '';

            if($category) {
                $categoryQuery = ' AND category = ' . $category;
            }

            $stmt = $this->db->prepare("SELECT id FROM products WHERE deleted = 0".$categoryQuery);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('products', 'Product::getAllCount', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function getFeatured() {
            $stmt = $this->db->prepare("SELECT prod.* FROM featured_products fprod JOIN products prod ON 
                                            fprod.product = prod.id WHERE prod.deleted = 0");
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('featured_products, products', 'Product::getFeatured', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getData($id) {
            $stmt = $this->db->prepare("SELECT * FROM products WHERE id = :id");
            $stmt->bindParam(":id", $id);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('products', 'Product::getData', $stmt->errorInfo());
            }

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        public function getAllCategories() {
            $stmt = $this->db->prepare("SELECT * FROM category");
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('category', 'Product::getAllCategories', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function getCategoryName($id) {
            $stmt = $this->db->prepare("SELECT name FROM category WHERE id = :id");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('category', 'Product::getCategoryName', $stmt->errorInfo());
            }

            return $stmt->fetch(PDO::FETCH_ASSOC)['name'];
        }

        public function getSimilar($limit, $not) {
            $stmt = $this->db->prepare("SELECT * FROM products WHERE id <> :id ORDER BY RAND() LIMIT :limit");
            $stmt->bindParam(":id", $not, PDO::PARAM_INT);
            $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('products', 'Product::getSimilar', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

        public function isInBasket($product, $user) {
            $stmt = $this->db->prepare("SELECT id FROM basket WHERE product = :product AND user = :user");
            $stmt->bindParam(":product", $product, PDO::PARAM_INT);
            $stmt->bindParam(":user", $user, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('basket', 'Product::isInBasket', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function removeFromBasket($product, $user) {
            $stmt = $this->db->prepare("DELETE FROM basket WHERE product = :product AND user = :user");
            $stmt->bindParam(":product", $product, PDO::PARAM_INT);
            $stmt->bindParam(":user", $user, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('basket', 'Product::removeFromBasket', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function addToBasket($product, $user) {
            $stmt = $this->db->prepare("INSERT INTO basket (product, user) VALUES (:product, :user)");
            $stmt->bindParam(":product", $product, PDO::PARAM_INT);
            $stmt->bindParam(":user", $user, PDO::PARAM_INT);
            $stmt->execute();

            if ($stmt->errorInfo()[0] != '00000') {
                $this->err->saveError('basket', 'Product::addToBasket', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }
    }
