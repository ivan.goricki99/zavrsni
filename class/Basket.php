<?php

    class Basket {
        private $db;
        private $error;

        function __construct($pdo, $error){
            $this->db = $pdo;
            $this->err = $error;
        }

        public function add($productId, $userId) {
            $stmt = $this->db->prepare("INSERT INTO basket (user, product) VALUES (:user, :product)");
            $stmt->bindParam(":user", $userId, PDO::PARAM_INT);
            $stmt->bindParam(":product", $productId, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('basket', 'Basket::add', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function remove($productId, $userId) {
            $stmt = $this->db->prepare("DELETE FROM basket WHERE product = :product AND user = :user");
            $stmt->bindParam(":user", $userId, PDO::PARAM_INT);
            $stmt->bindParam(":product", $productId, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('basket', 'Basket::remove', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function isInBasket($productId, $userId) {
            $stmt = $this->db->prepare("SELECT id FROM basket WHERE product = :product AND user = :user");
            $stmt->bindParam(":user", $userId, PDO::PARAM_INT);
            $stmt->bindParam(":product", $productId, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('basket', 'Basket::isInBasket', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function getAllProducts($userId, $limit, $offset) {
            $stmt = $this->db->preapre("SELECT * FROM basket WHERE user = :user LIMIT :offset, :limit");
            $stmt->bindParam(":user", $userId, PDO::PARAM_INT);
            $stmt->bindParam(":offset", $offset, PDO::PARAM_INT);
            $stmt->bindParam(":limit", $limit, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('basket', 'Basket::getAllProducts', $stmt->errorInfo());
            }

            return $stmt->fetchAll(PDO::FETCH_ASSOC);
        }

    }

