<?php

    class User {
        private $db;
        private $error;

        function __construct($pdo, $error){
            $this->db = $pdo;
            $this->err = $error;
        }

        public function isLoggedin() {
            return isset($_SESSION['user']);
        }

        public function login($username, $password) {
            $password = sha1($password);

            $stmt = $this->db->prepare("SELECT id FROM users WHERE username = :username AND password = :password");
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::login', $stmt->errorInfo());
            }

            return $stmt->fetch(PDO::FETCH_ASSOC)['id'];
        }

        public function registration($mail, $username, $password) {
            $password = sha1($password);

            $stmt = $this->db->prepare("INSERT INTO users (username, password, mail) VALUES (:username, :password, :mail)");
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->bindParam(":password", $password, PDO::PARAM_STR);
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::registration', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function getData($userID) {
            $stmt = $this->db->prepare("SELECT * FROM users WHERE id = :id");
            $stmt->bindParam(":id", $userID, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::getData', $stmt->errorInfo());
            }

            return $stmt->fetch(PDO::FETCH_ASSOC);
        }

        public function exist($id) {
            $stmt = $this->db->prepare("SELECT id FROM users WHERE id = :id");
            $stmt->bindParam(":id", $id, PDO::PARAM_INT);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::exist', $stmt->errorInfo());
            }

            $returnedId = $stmt->rowCount();

            return ($returnedId ? true : false);
        }

        public function emailExist($mail) {
            $stmt = $this->db->prepare("SELECT id FROM users WHERE mail = :mail");
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::emailExist', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function usernameExist($username) {
            $stmt = $this->db->prepare("SELECT id FROM users WHERE username = :username");
            $stmt->bindParam(":username", $username, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('users', 'User::usernameExist', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function subscribe($mail) {
            $stmt = $this->db->prepare("INSERT INTO subscribe (mail) VALUES (:mail)");
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('subscribe', 'User::subscribe', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }

        public function isSubscribed($mail) {
            $stmt = $this->db->prepare("SELECT id FROM subscribe WHERE mail = :mail");
            $stmt->bindParam(":mail", $mail, PDO::PARAM_STR);
            $stmt->execute();

            if($stmt->errorInfo()[0] != '00000'){
                $this->err->saveError('subscribe', 'User::isSubscribed', $stmt->errorInfo());
            }

            return $stmt->rowCount();
        }
    }
