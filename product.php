<?php
    include 'core/init.php';

    if(isset($_GET['id']) && !empty($_GET['id']) && $data = $product->getData($_GET['id'])){
        $id = $_GET['id'];
        $similar = $product->getSimilar(9, $id);

        include 'pages/products/product.php';
    }else {
        include 'pages/products/not_found.php';
    }
