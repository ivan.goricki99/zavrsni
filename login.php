<?php
    include 'core/init.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">

        <title>Prijava</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <link rel="manifest" href="manifest.json">

        <script src="assets/js/containerControl.js"></script>
        <script src="assets/js/fetchApi.js"></script>
        <script>
            if(containerControl.isLoggedIn()) {
                containerControl.redirect('user.php');
            }
        </script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="caption">
                            <h2>Prijavi se</h2>
                            <div class="line-dec"></div>
                            <form id="login_form">
                                <ul class="list-group">
                                    <li>
                                        <p class="mb-0">Korisničko ime</p>
                                        <input type="text"
                                               id="username-login-input"
                                               class="form-input form-control"
                                               name="username"
                                               placeholder="Unesite korisničko ime">
                                    </li>
                                    <li>
                                        <p class="mb-0">Lozinka</p>
                                        <input type="password"
                                               id="password-login-input"
                                               class="form-input form-control"
                                               name="password"
                                               placeholder="Unesite lozinku">
                                    </li>
                                    <li>
                                        <button type="submit"
                                                id="submit-login-button"
                                                class="element__margin-top_bottom-10 main-button">Prijavi se</button>
                                    </li>
                                </ul>
                                <div id="login-error-container"></div>
                            </form>
                            <p class="registration-info">Nemate račun? <a href="registration.php">Registriraj se</a>!</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>

        <script>
            document.getElementById('login_form').addEventListener('submit', function (e) {
                e.preventDefault();
                let username = document.getElementById('username-login-input').value;
                let password = document.getElementById('password-login-input').value;
                let errorContainer = document.getElementById('login-error-container');

                containerControl.empty(errorContainer);

                if(username && password){
                    postData('process/login.php', {
                            "method": "login",
                            "username": username,
                            "password": password
                    }).then(data => {
                        if(data.success){
                            document.cookie = "user=" + data.success;
                            containerControl.redirect('index.php');
                        }else{
                            errorContainer.innerHTML = '<p>' + data.message + '</p>';
                        }
                    });
                }else {
                    errorContainer.innerHTML = '<p>Korisničko ime i lozinka moraju biti uneseni.</p>';
                }
            });

            containerControl.navElementInit();
        </script>
    </body>
</html>
