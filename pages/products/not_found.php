<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">
        <link rel="manifest" href="manifest.json">

        <title>Proizvod nije pronađen</title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/owl.css">

        <script src="assets/js/containerControl.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="banner">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="caption">
                            <h2>Proizvod nije pronađen!</h2>
                            <div class="line-dec"></div>
                            <p>Proizvod koji tražite ne postoji. Provjerite jeste li učitali ispravnu popveznicu.</p>
                            <div class="main-button">
                                <a href="products.php">Pogledaj ostale proizvode</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="featured-items">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Istaknuti proizvodi</h1>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="owl-carousel owl-theme">
                            <?php
                            $featuredProducts = $product->getFeatured();

                            foreach ($featuredProducts as $featuredProduct){
                                echo '
                                                <a href="product.php?id='.$featuredProduct['id'].'">
                                                    <div class="featured-item">
                                                        <img src="assets/images/products/'.getFirst($featuredProduct['image']).'">
                                                        <h4>'.$featuredProduct['title'].'</h4>
                                                        <h6>'.$featuredProduct['price'].' kn</h6>
                                                    </div>
                                                </a>
                                            ';
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>

        <script>
            containerControl.navElementInit();
        </script>
    </body>
</html>