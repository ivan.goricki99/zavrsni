<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="theme-color" content="#3a8bcd">
        <link rel="manifest" href="manifest.json">

        <title><?php echo $data['title']; ?>></title>

        <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700" rel="stylesheet">
        <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/css/fontawesome.css">
        <link rel="stylesheet" href="assets/css/tooplate-main.css">
        <link rel="stylesheet" href="assets/css/owl.css">
        <link rel="stylesheet" href="assets/css/flex-slider.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <script src="assets/js/containerControl.js"></script>
        <script src="assets/js/fetchApi.js"></script>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark static-top">
            <div class="container">
                <a class="navbar-brand" href="#"><img src="assets/images/header-logo.png" alt=""></a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul id="navUl" class="navbar-nav ml-auto">
                        <li class="nav-item">
                            <a class="nav-link" href="index.php">Naslovna</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="products.php">Proizvodi</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div class="single-product">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="product-slider">
                            <div id="slider" class="flexslider">
                                <ul class="slides">
                                    <li>
                                        <img src="assets/images/products/<?php echo $data['image']; ?>" />
                                    </li>
                                </ul>
                            </div>
                            <div id="carousel" class="flexslider">
                                <ul class="slides">
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="right-content">
                            <h4><?php echo $data['title']; ?></h4>
                            <h6><?php echo $data['price']; ?> kn</h6>
                            <p><?php echo $data['description']; ?></p>
                            <span><?php echo $data['quantity']; ?> na zalihama</span>
                            <form action="" method="get">
                                <label for="quantity">Količina:</label>
                                <input name="quantity" type="quantity" class="quantity-text" id="quantity"
                                       onfocus="if(this.value == '1') { this.value = ''; }"
                                       onBlur="if(this.value == '') { this.value = '1';}"
                                       value="1">
                                <input type="button" onclick="addToBasket(<?php echo $data['id']; ?>)" id="add-basket" class="button add-basket displayNone" value="Dodaj u košaricu!">
                                <input type="button" onclick="removeFromBasket(<?php echo $data['id']; ?>)" id="remove-basket" class="button remove-basket displayNone" value="Makni iz košarice!">
                            </form>
                            <div class="down-content">
                                <div class="categories">
                                    <h6>Kategorija: <span><a href="products.php?cat=<?php echo $data['category']; ?>"><?php echo $product->getCategoryName($data['category']); ?></a></span></h6>
                                </div>
                                <div class="share">
                                    <h6>Podijeli: <span><a href="#"><i class="fa fa-facebook"></i></a><a href="#"><i class="fa fa-linkedin"></i></a><a href="#"><i class="fa fa-twitter"></i></a></span></h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="featured-items">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Slični proizvodi</h1>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div id="product-container" class="owl-carousel owl-theme"></div>
                    </div>
                </div>
            </div>
        </div>

        <div class="subscribe-form">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="section-heading">
                            <div class="line-dec"></div>
                            <h1>Pretplati se sada!</h1>
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-2">
                        <div class="main-content">
                            <p>Kako bi u što kraćem roku saznao za nove proizvode na našem shopu pretplati se i svakog dana dobijav mail o novim proizvodima raspoloživim na našem webshopu</p>
                            <div class="container">
                                <form id="subscribe" action="" method="get">
                                    <div class="row">
                                        <div class="col-md-7">
                                            <fieldset>
                                                <input name="email" type="text" class="form-control" id="email"
                                                       onfocus="if(this.value == 'E-mail adresa...') { this.value = ''; }"
                                                       onBlur="if(this.value == '') { this.value = 'E-mail adresa...';}"
                                                       value="E-mail adresa..." required="">
                                            </fieldset>
                                        </div>
                                        <div class="col-md-5">
                                            <fieldset>
                                                <button type="submit" id="form-submit" class="button">PRetplati se</button>
                                            </fieldset>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="logo">
                            <img src="assets/images/header-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="footer-menu">
                            <ul>
                                <li><a href="index.php">Naslovna</a></li>
                                <li><a href="products.php">Proizvodi</a></li>
                                <li><a href="aboutus.php">O nama</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="social-icons">
                            <ul>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="sub-footer">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright-text">
                            <p>Copyright &copy; 2021 Ivan Gorički
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="assets/js/custom.js"></script>
        <script src="assets/js/owl.js"></script>
        <script src="assets/js/isotope.js"></script>
        <script src="assets/js/flex-slider.js"></script>
        <script>
            const urlParams = new URLSearchParams(window.location.search);

            containerControl.navElementInit();
            containerControl.initBasketButton(urlParams.get('id'));
            containerControl.loadSimilar(urlParams.get('id'));

            function removeFromBasket(product) {
                containerControl.removeFromBasket(product);
            }

            function addToBasket(product) {
                containerControl.addToBasket(product);
            }
        </script>
    </body>
</html>