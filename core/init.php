<?php
    session_start();
    date_default_timezone_set('Europe/Zagreb');
    mb_internal_encoding('UTF-8');

    define('ROOT', $_SERVER['DOCUMENT_ROOT'].'/zavrseni/');

    require_once ROOT.'/config/db_config.php';

    require_once ROOT.'helpers/functions.php';
    require_once ROOT.'helpers/rest_functions.php';

    require_once ROOT.'core/connection.php';

    require_once ROOT.'class/Errors.php';
    require_once ROOT.'class/Product.php';
    require_once ROOT.'class/Basket.php';
    require_once ROOT.'class/User.php';

    $errorsInstance = new Errors($pdo);
    $user = new User($pdo, $errorsInstance);
    $product = new Product($pdo, $errorsInstance);
    $basket = new Basket($pdo, $errorsInstance);

    if($user->isLoggedin()) {
        $_userData = $user->getData($_SESSION['user']);
    }
