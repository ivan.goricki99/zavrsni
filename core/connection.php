<?php
    $dns = 'mysql:dbname='.$_dbConfig['dns']['dbname'].';host='.$_dbConfig['dns']['host'].';charset='.$_dbConfig['dns']['charset'];
    $user = $_dbConfig['user'];
    $password = $_dbConfig['password'];

    try{
        $pdo = new PDO($dns, $user, $password);
    }catch(PDOexception $e){
        echo 'Povezivanje sa serverom nije moguće. ['.$e->getMessage().']';
    }
